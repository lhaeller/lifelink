# lifelink

*"if there's an API you should be enabled to link it"*

**The Mission**: To build a platform where satisfaction with life can be linked to data from all kinds of apps and services.

**The Vision**: To enable everyone to respond to their life with data-driven decisions.